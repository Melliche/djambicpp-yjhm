import piece.king
import variable
from board import Board
import pygame
from player import Player
from random import randint
from piece import king, assassin, necromobil, reporter, militant, diplomat


class GameInit:
    """ Class to init the game """
    colorsParse = {"B": "Blue",
                   "Y": "Yellow",
                   "O": "Orange",
                   "G": "Green"}
    peonsParse = {"K": king.King,
                  "A": assassin.Assassin,
                  "M": militant.Militant,
                  "R": reporter.Reporter,
                  "D": diplomat.Diplomat,
                  "N": necromobil.Necromobil,
                  }

    def __init__(self, number_players):

        self.board = Board(9)
        self.nbrPlayers = number_players
        self.backgroundColor = (255, 255, 255)
        self.width = 1400
        self.height = 1080
        self.gameEnded = False
        self.totalKingDead = 0
        self.number_players = number_players
        self.list_players = []

    def generatePlayers(self):
        """ Generate players with random order of colors"""
        players_colors = ["Orange", "Blue", "Green", "Yellow"]

        for color in players_colors:
            player = Player(color)
            self.list_players.append(player)

    def generatePeons(self):
        """ Generate peons on the board """
        for i, line in enumerate(self.board.board):
            for j, cell in enumerate(line):
                if len(cell) == 2:
                    color = self.colorsParse[cell[0]]
                    classname = self.peonsParse[cell[1]]
                    player = self.getPlayerByColor(color)
                    player.peons.append(classname(i, j, color, variable.color_map[cell[0]], cell[1], cell[0]))

    def getPlayerByColor(self, color):
        if color is None:
            return "No color found"
        for player in self.list_players:
            if player.color == color:
                return player

    def getPionOnBoard(self, x, y):
        pion = self.board.board[x][y]
        if len(pion) == 2:
            pionLetter = pion[1]
            color = self.colorsParse[pion[0]]
            player = self.getPlayerByColor(color)
            for peon in player.peons:
                if peon.letter == pionLetter:
                    peon.color = color
                    peon.positionX = x
                    peon.positionY = y
                    return peon
        else:
            return {"case": '_', "posX": x, "posY": y}

    def handleDirection(self, peon, direction, n=1):
        cell = None
        x, y = peon.positionX + direction[0] * n, peon.positionY + direction[1] * n
        is_not_out_of_board = (0 <= x <= 8 and 0 <= y <= 8) and (n <= peon.maxRange)
        if is_not_out_of_board:
            cell = self.board.board[x][y]
        isNotBlocked = (cell == '_')
        record = [{"x": x, "y": y, "isEmpty": isNotBlocked}]
        if isNotBlocked:
            record.extend(self.handleDirection(peon, direction, n + 1))
            return record
        return record if cell else []

    def updateBoard(self, peon):
        for pos in peon.possiblePositionToMove:
            if pos['isEmpty']:
                print("X")
                # self.board.board[pos['x']][pos['y']] = 'X'
            # if pos['isEmpty'] is False:
            #     self.board.board[pos['x']][pos['y']] = 'U'

    def handleMove(self, peon):
        peon.possiblePositionToMove = []

        for direction in variable.directions:
            peon.possiblePositionToMove.extend(self.handleDirection(peon, direction))

        # print(peon.possiblePositionToMove)
        # print(self.board.printBoard(peon))

        # self.board.printBoard()
