import pygame

WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)

CELL_SIZE = 75
NUM_ROWS = 9
NUM_COLS = 9

color_map = {
    'B': (0, 0, 255),  # Blue
    'G': (0, 255, 0),  # Green
    'Y': (255, 255, 0),  # Yellow
    'O': (255, 165, 0),  # Orange
    'L': (128, 128, 128),  # Grey for the central cell
    # 'O': (0, 0, 0),  # Orange
    '_': None  # No color
}

directions = [
    (-1, 0),  # Up
    (1, 0),  # Down
    (0, -1),  # Left
    (0, 1),  # Right
    (-1, -1),  # Up-Left
    (-1, 1),  # Up-Right
    (1, -1),  # Down-Left
    (1, 1)  # Down-Right
]
