from enum import Enum
class TeamColor(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3
    YELLOW = 4