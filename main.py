import pygame

from gameInit import GameInit
from view.gameView import GameView


def main():
    game = GameInit(4)
    game.generatePlayers()
    game.generatePeons()

    pygame.init()

    game_instance = GameView(game)
    game_instance.displayGame()


if __name__ == '__main__':
    main()
