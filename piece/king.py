from pion import Pion


class King(Pion):
    """This class is the king class, it inherits from the Pion class. It has the following attributes:"""

    def __init__(self, positionX, positionY, color, board_color, type, team):
        super().__init__(positionX, positionY, color, board_color)
        self.positionY = positionY
        self.positionX = positionX
        self.board_color = board_color
        self.letter = 'K'
        self.color = color
        self.canKill = True
        self.type = type
        self.image = None
        self.team = team
        self.rect = None
        self.dragging = False


    def ifDead(self, board):
        # Parcourir le plateau
        for row in board.board:
            for piece in row:
                # Vérifier si la pièce est de la même couleur que le roi
                if isinstance(piece, Pion) and piece.color != self.color:
                    piece.canMove = False




