from pion import Pion


class Reporter(Pion):
    def __init__(self, positionX, positionY, color, board_color, type, team):
        super().__init__(positionX, positionY, color, board_color)
        self.positionY = positionY
        self.positionX = positionX
        self.board_color = board_color
        self.color = color
        self.letter = 'R'
        self.canKill = True
        self.type = type
        self.image = None
        self.team = team
        self.rect = None
        self.dragging = False
        self.positionXError = None

