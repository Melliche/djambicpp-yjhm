from pion import Pion
from board import Board

class EmptyCase(Pion):
    def __init__(self, color, team, type, rect, image, positionX, positionY):
        super().__init__(positionX, positionY, color)
        self.positionY = 0
        self.positionX = 0
        self.color = color
        self.letter = 'D'
        self.canKill = False
        self.type = type
        self.image = image
        self.team = team
        self.rect = rect
        self.dragging = False
        self.positionXError = None

