class Pion:
    # def __init__(self, positionX, positionY, color):
    def __init__(self, positionX, positionY, color, board_color):
        self.color = color
        self.board_color = board_color
        self.hasMoved = False
        self.canMove = False
        self.canAttack = False
        self.canBeAttacked = False
        self.isAlive = True
        self.possiblePositionToMove = []
        self.maxRange = 8
        self.positionX = positionX
        self.positionY = positionY

    def __repr__(self):
        return f"{self.__class__.__name__}\n"
