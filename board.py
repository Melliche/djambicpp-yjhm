from copy import copy

class Board:
    def __init__(self, size):
        self.board = []
        self.size = size
        with open("board.txt", "r") as f:
            for line in f:
                cells = line.strip().split(' ')
                self.board.append(cells)
        # self.printBoard()

    def printBoard(self):
        for i in range(0, int(len(self.board))):
            print(self.board[i])

    def get_board(self):
        return self.board

    def __str__(self):
        return '\n'.join([' '.join(map(str, row)) for row in self.board])

    def place_piece(self, piece, x, y):
        self.board[x][y] = piece
