import pygame as py
from variable import WHITE


class Screen:
    def __init__(self, title, width=800, height=600):
        # HEIGHT OF A WINDOW
        self.screen = None
        self.height = height
        # TITLE OF A WINDOW
        self.title = title
        # WIDTH OF A WINDOW
        self.width = width
        # COLOUR CODE
        self.fill = WHITE
        # CURRENT STATE OF A SCREEN
        self.CurrentState = False

    def draw(self, x, y):
        print("Drawing at x: %d, y: %d" % (x, y))

    def makeCurrentScreen(self):
        py.display.set_caption(self.title)
        self.CurrentState = True
        self.screen = py.display.set_mode((self.width, self.height))

    def endCurrentScreen(self):
        self.CurrentState = False

    def checkUpdate(self, fill):
        self.fill = fill
        return self.CurrentState

    def screenUpdate(self):
        if self.CurrentState:
            self.screen.fill(self.fill)

    def returnTitle(self):
        return self.title

    def getScreen(self):
        return self.screen
