import pygame

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
screen = pygame.display.set_mode((800, 600))


class Button:
    def __init__(self, color, x, y, width, height, text=''):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text
        self.selected = False

    def draw(self, screen):
        pygame.draw.rect(screen, self.color, (self.x, self.y, self.width, self.height), 0)
        if self.selected:
            pygame.draw.rect(screen, BLACK, (self.x, self.y, self.width, self.height), 2)
        if self.text != '':
            font = pygame.font.SysFont('comicsans', 30)
            text_surface = font.render(self.text, True, BLACK)
            screen.blit(text_surface, (self.x + (self.width / 2 - text_surface.get_width() / 2),
                                       self.y + (self.height / 2 - text_surface.get_height() / 2)))

    def isOver(self, pos):
        if self.x < pos[0] < self.x + self.width and self.y < pos[1] < self.y + self.height:
            return True
        else:
            return False
