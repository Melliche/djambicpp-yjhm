import pygame

import variable
from board import Board
from piece.emptycase import EmptyCase
from pion import Pion
from view.pygameboard import generateButtons, handle_button_click
from view.screen import Screen

pawn_images = {
    'K': pygame.transform.scale(pygame.image.load('./images/king.png').convert_alpha(),
                                (variable.CELL_SIZE, variable.CELL_SIZE)),
    'A': pygame.transform.scale(pygame.image.load('./images/assassin.png').convert_alpha(),
                                (variable.CELL_SIZE, variable.CELL_SIZE)),
    'D': pygame.transform.scale(pygame.image.load('./images/diplomate.png').convert_alpha(),
                                (variable.CELL_SIZE, variable.CELL_SIZE)),
    'R': pygame.transform.scale(pygame.image.load('./images/reporter.png').convert_alpha(),
                                (variable.CELL_SIZE, variable.CELL_SIZE)),
    'M': pygame.transform.scale(pygame.image.load('./images/militant.png').convert_alpha(),
                                (variable.CELL_SIZE, variable.CELL_SIZE)),
    'N': pygame.transform.scale(pygame.image.load('./images/necromobile.png').convert_alpha(),
                                (variable.CELL_SIZE, variable.CELL_SIZE)),
}


class GameView:
    def __init__(self, game_init):
        # Initialisation de l'écran de jeu.
        self.carried_pawn = None
        self.screen_obj = Screen("Djambi", 800, 600)
        self.screen_obj.makeCurrentScreen()

        # Variables de contrôle du jeu.
        self.game_is_running = True
        self.clock = pygame.time.Clock()
        self.start_game = False
        self.show_buttons = True

        # Initialisation du plateau et des pions.
        self.game_init = game_init
        self.pawns = []

    def handle(self):
        buttons = generateButtons(self.screen_obj.screen, self.show_buttons)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.game_is_running = False

            if event.type == pygame.MOUSEBUTTONDOWN:
                if buttons:
                    self.menuButtonsClick(buttons)
                if event.button == 1 and self.start_game and not self.carried_pawn:  # Clic gauche
                    self.selectPion(event)
                if self.carried_pawn:
                    self.drop_carried_pawn(event)

            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1 and not self.carried_pawn:  # Relâchement du clic gauche
                    self.dropPion(event)

            elif event.type == pygame.MOUSEMOTION:  # Drag du pion
                self.dragPion(event)
        pass

    def init_djambi_boardView(self):
        pawns = []
        for player in self.game_init.list_players:
            for pawn in player.peons:
                # print(pawn.positionX, pawn.positionY, pawn.color, pawn.type, " type")
                pawn_rect = pygame.Rect(pawn.positionY * variable.CELL_SIZE, pawn.positionX * variable.CELL_SIZE,
                                        variable.CELL_SIZE,
                                        variable.CELL_SIZE)
                image = pawn_images.get(pawn.letter)
                pawn.image = image
                pawn.rect = pawn_rect
                pawns.append(pawn)
        self.pawns = pawns

    # Gestion des interactions utilisateur.

    def drop_carried_pawn(self, event):

        # définit la position initiale du pion tué
        self.carried_pawn.initial_x, self.carried_pawn.initial_y = self.carried_pawn.positionX, self.carried_pawn.positionY

        # Coordonnées tentées pour déposer le pion
        attempted_x, attempted_y = event.pos[1] + self.carried_pawn.positionX, event.pos[
            0] + self.carried_pawn.positionY

        # Nouvelles coordonnées du pion
        adjusted_x, adjusted_y = self.snap_to_grid(attempted_x, attempted_y)

        # Vérifiez si l'emplacement est valide pour déposer le pion
        if self.is_valid_drop_location(adjusted_x // variable.CELL_SIZE, adjusted_y // variable.CELL_SIZE):
            self.carried_pawn.positionX, self.carried_pawn.positionY = adjusted_x // variable.CELL_SIZE, adjusted_y // variable.CELL_SIZE
            self.carried_pawn.rect.x, self.carried_pawn.rect.y = adjusted_y, adjusted_x
            self.game_init.board.board[self.carried_pawn.positionX][
                self.carried_pawn.positionY] = self.carried_pawn.team + self.carried_pawn.type
            self.carried_pawn = None

    def is_valid_drop_location(self, x, y):
        if not (0 <= x < variable.NUM_COLS and 0 <= y < variable.NUM_ROWS):
            return False
        cell = self.game_init.board.board[x][y]
        return cell == '_' and not cell == 'O'

    # Gestion des clics sur les boutons du menu.
    def menuButtonsClick(self, buttons):
        handle_button_click(self.screen_obj.screen,
                            lambda status: setattr(self, 'start_game', status))
        if buttons[-1].isOver(pygame.mouse.get_pos()):
            self.start_game = True
            self.startGame()

    # Sélectionne un pion lorsqu'il est cliqué
    def selectPion(self, event):
        for pawn in self.pawns:
            if pawn.rect is None or not pawn.isAlive:
                continue
            if pawn.rect.collidepoint(event.pos):
                pawn.dragging = True
                self.game_init.handleMove(pawn)
                pawn.positionXError, pawn.positionYError = pawn.positionX, pawn.positionY

                # Enregistre les coordonnées initiales du pion et la position relative de la souris
                pawn.initial_x, pawn.initial_y = pawn.rect.y, pawn.rect.x
                pawn.positionX, pawn.positionY = (pawn.rect.y - event.pos[1]), (pawn.rect.x - event.pos[0])
                break

    def dropPion(self, event):
        for pawn in self.pawns:
            if pawn.dragging:
                pawn.dragging = False

                # pawn.initial_y, pawn.initial_x = pawn.positionY, pawn.positionX

                # Coordonnées tentées pour le dépôt du pion
                attempted_x, attempted_y = event.pos[1] + pawn.positionX, event.pos[0] + pawn.positionY
                adjusted_x, adjusted_y = self.snap_to_grid(attempted_x, attempted_y)

                target_pawn = self.find_pawn_at_position(adjusted_x // variable.CELL_SIZE,
                                                         adjusted_y // variable.CELL_SIZE)

                # Vérifie si un pion ennemi est présent sur la case cible
                if target_pawn and target_pawn.color != pawn.color:
                    print(f"Pion ennemi {target_pawn} tué et ramassé")
                    target_pawn.positionYError, target_pawn.positionXError = None, None
                    target_pawn.isAlive = False
                    # Déplace le pion attaquant sur la position du pion ennemi
                    self.move_pawn(pawn, adjusted_x, adjusted_y)
                    # Ramasse automatiquement le pion ennemi tué
                    self.start_carrying(target_pawn)
                else:
                    print(
                        f"Pion {pawn} tenté déposé à la position {adjusted_x // variable.CELL_SIZE}, {adjusted_y // variable.CELL_SIZE}.")
                    if self.can_move(pawn, adjusted_x // variable.CELL_SIZE, adjusted_y // variable.CELL_SIZE):
                        self.move_pawn(pawn, adjusted_x, adjusted_y)
                    else:
                        if pawn.positionXError and not self.carried_pawn and pawn.positionYError:
                            pawn.positionX, pawn.positionY = pawn.positionXError, pawn.positionYError
                            pawn.rect.x, pawn.rect.y = pawn.positionYError * variable.CELL_SIZE, pawn.positionXError * variable.CELL_SIZE
                break

    def move_pawn(self, pawn, x, y):
        # Met à jour la position du pion dans le modèle de jeu
        pawn.rect.x, pawn.rect.y = y, x

        self.game_init.board.board[pawn.initial_x // variable.CELL_SIZE][
            pawn.initial_y // variable.CELL_SIZE] = '_'

        pawn.positionX, pawn.positionY = x // variable.CELL_SIZE, y // variable.CELL_SIZE
        self.game_init.board.board[pawn.positionX][
            pawn.positionY] = pawn.team + pawn.type

        pawn.initial_y, pawn.initial_x = pawn.positionY * variable.CELL_SIZE, pawn.positionX * variable.CELL_SIZE
        print(pawn.positionX, pawn.positionY, " position actualisé")
        # print(self.game_init.board.printBoard())
        # print(f"Pion {pawn} déplacé à la position {pawn.positionX}, {pawn.positionY}.")

    def find_pawn_at_position(self, x, y):
        for pawn in self.pawns:
            if pawn.positionX == x and pawn.positionY == y and pawn.isAlive:
                return pawn
        return None

    def start_carrying(self, pawn):
        # Marquez le pion comme étant porté
        self.carried_pawn = pawn
        self.carried_pawn.dragging = True

    def can_move(self, pawn, x, y):
        new_move = {'x': x, 'y': y, "isEmpty": True}
        print(new_move, " new move")
        if new_move in pawn.possiblePositionToMove:
            return True
        return False

    def dragPion(self, event):
        for pawn in self.pawns:
            if pawn.dragging:
                # Nouvelles coordonnées du pion
                new_x, new_y = event.pos[0] + pawn.positionX, event.pos[1] + pawn.positionY

                # Ajustement des coordonnées pour qu'elles correspondent à la grille du plateau de jeu
                new_x = max(0, min(variable.NUM_COLS * variable.CELL_SIZE - variable.CELL_SIZE, new_x))
                new_y = max(0, min(variable.NUM_ROWS * variable.CELL_SIZE - variable.CELL_SIZE, new_y))

                # Mise à jour des coordonnées du pion
                pawn.rect.x, pawn.rect.y = new_x, new_y

    # Commence la partie, ajuste l'écran et initialise les pions.
    def startGame(self):
        self.game_is_running = True
        self.start_game = True
        self.show_buttons = False
        self.screen_obj = Screen("Jeu en cours", width=variable.NUM_COLS * variable.CELL_SIZE,
                                 height=variable.NUM_ROWS * variable.CELL_SIZE)
        self.screen_obj.makeCurrentScreen()
        self.init_djambi_boardView()

    # Affiche le plateau de jeu et les pions.
    def displayBoard(self):
        self.screen_obj.screen.fill(variable.WHITE)
        pawn_in_drag = None
        for row in range(variable.NUM_ROWS):  # y
            for col in range(variable.NUM_COLS):
                rect = pygame.Rect(col * variable.CELL_SIZE, row * variable.CELL_SIZE, variable.CELL_SIZE,
                                   variable.CELL_SIZE)
                if self.game_init.board.board[col][row] == 'L':
                    pygame.draw.rect(self.screen_obj.screen, variable.color_map['L'],
                                     rect)  # Utiliser la couleur de "L"
                # elif self.game_init.board.board[row][col] == 'O':  # Si la case est un obstacle (pion mort)
                #     pygame.draw.rect(self.screen_obj.screen, pygame.Color('Black'),
                #                      rect)  # Utiliser une couleur grise pour les obstacles

                else:
                    pygame.draw.rect(self.screen_obj.screen, variable.BLACK, rect,
                                     1)  # Dessiner les bords pour les autres cases

        for pawn in self.pawns:
            if not pawn.dragging:
                if pawn.rect is None:
                    continue
                if pawn.isAlive:
                    pygame.draw.rect(self.screen_obj.screen, pawn.color, pawn.rect)  # Fond du pion
                else:
                    pygame.draw.rect(self.screen_obj.screen, variable.BLACK, pawn.rect)  # Fond du pion

                if pawn.image and pawn.isAlive:  # Image du pion
                    image_rect = pawn.image.get_rect(center=pawn.rect.center)
                    self.screen_obj.screen.blit(pawn.image, image_rect)
            else:
                pawn_in_drag = pawn  # Gardez une référence pour le pion en déplacement

        # Ensuite, si un pion est en déplacement, index hauteur max
        if pawn_in_drag:
            pygame.draw.rect(self.screen_obj.screen, pawn_in_drag.color,
                             pawn_in_drag.rect)  # Fond du pion en déplacement
            if pawn_in_drag.image:  # Image du pion en déplacement
                image_rect = pawn_in_drag.image.get_rect(center=pawn_in_drag.rect.center)
                self.screen_obj.screen.blit(pawn_in_drag.image, image_rect)

    # Boucle principale du jeu.
    def displayGame(self):
        while self.game_is_running:
            self.screen_obj.screenUpdate()
            self.handle()
            if self.start_game:
                self.displayBoard()
            pygame.display.flip()
            self.clock.tick(60)

    # Ajuste les coordonnées pour qu'elles correspondent à la grille du plateau de jeu.
    def snap_to_grid(self, x, y):
        grid_x = (round(x / variable.CELL_SIZE) * variable.CELL_SIZE) + variable.CELL_SIZE // 2 - (
                variable.CELL_SIZE // 2)
        grid_y = (round(y / variable.CELL_SIZE) * variable.CELL_SIZE) + variable.CELL_SIZE // 2 - (
                variable.CELL_SIZE // 2)
        # Clipping pour s'assurer que les coordonnées sont à l'intérieur des limites du plateau
        grid_x = max(0, min(grid_x, (variable.NUM_COLS - 1) * variable.CELL_SIZE))
        grid_y = max(0, min(grid_y, (variable.NUM_ROWS - 1) * variable.CELL_SIZE))
        return grid_x, grid_y
