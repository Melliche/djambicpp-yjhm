import pygame
from view.button import Button

WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

button_2_players = Button(GREEN, 150, 450, 200, 50, '2 Joueurs')
button_2_players.selected = True
button_4_players = Button(GREEN, 450, 450, 200, 50, '4 Joueurs')
start_button = Button(BLUE, 300, 520, 200, 50, 'Start')
selected_players = 2


def handle_button_click(screen, start_game_status):
    global selected_players
    pos = pygame.mouse.get_pos()

    for button in [button_2_players, button_4_players]:
        if button.isOver(pos):
            button.selected = True
            if button == button_2_players:
                button_4_players.selected = False
                selected_players = 2
            else:
                button_2_players.selected = False
                selected_players = 4
        button.draw(screen)

    if start_button.isOver(pos):
        print(f"Le nombre de joueurs sélectionné est : {selected_players}")
        start_game_status(True)


def generateButtons(screen, show_buttons):
    if show_buttons:
        button_2_players.draw(screen)
        button_4_players.draw(screen)
        start_button.draw(screen)
        return [button_2_players, button_4_players, start_button]
    else:
        return []
